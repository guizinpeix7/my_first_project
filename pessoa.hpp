#include <iostream> 
#include <string> 

using namespace std; // Como isso funciona ?

class Pessoa{

//ATRIBUTOS
private:
	string nome,
	       idade,
	       telefone;
//METODOS CONTRUTORES E DESTRUTORES
public:
	Pessoa();
	~Pessoa(); //Como funciona o metodo contrutor e o metodo destrutor?

//METODOS ACESSORES

	string get_nome();
	void set_nome(string nome);

	string get_idade();
	void set_idade(string idade);

	std::string get_telefone();
	void  set_telefone(string telefone);

//FUNCOES ou METODOS
	void ligar();
};
