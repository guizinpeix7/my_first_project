#include <iostream>
#include "pessoa.hpp"

using namespace std; 

int main(){
	Pessoa pessoa1; 

	pessoa1.set_nome("Francisco");
	pessoa1.set_idade("50 anos");
	pessoa1.set_telefone("+55 (61) 991675903");

	cout << pessoa1.get_nome() << endl;
	cout << pessoa1.get_idade() << endl;
	cout << pessoa1.get_telefone() << endl;

}
